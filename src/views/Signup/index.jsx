import { Button, Container, TextField, withStyles } from "@material-ui/core";
import React, { Component, Fragment } from "react";
import Header from "../../components/Header";
import styles from "./styles";
import axios from "axios";
class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formValue: {
        taiKhoan: "",
        matKhau: "",
        hoTen: "",
        email: "",
        soDT: "",
        maNhom: "GP01",
      },
    };
  }
  handleChange = (event) => {
    //   console.log(event.target.name)
    // console.log(event.target.value);
    this.setState({
      formValue: {
        ...this.state.formValue,
        [event.target.name]: event.target.value,
      },
    });
  };

  handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const res = await axios({
        method: "POST",
        url: "https://elearning0706.cybersoft.edu.vn/api/QuanLyNguoiDung/DangKy",
        data: this.state.formValue,
      });
      console.log(res);
      alert("Dang ki thanh cong")

    //   <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
    //     <Alert onClose={handleClose} severity="success">
    //       This is a success message!
    //     </Alert>
    //   </Snackbar>
    } catch (err) {
      console.log(err);
    }
  };
  render() {
    const { formInput } = this.props.classes;
    return (
      <Fragment>
        <Header />
        <Container maxWidth="sm">
          <h1>Đăng Ký</h1>
          <form onSubmit={this.handleSubmit}>
            <div className={formInput}>
              <TextField
                //set value để đưa giá trị của state hiện lên trên input
                name="taiKhoan"
                onChange={this.handleChange}
                fullWidth
                label="Tài khoản"
                variant="outlined"
              />
            </div>
            <div className={formInput}>
              <TextField
                onChange={this.handleChange}
                name="matKhau"
                type="password"
                fullWidth
                label="Mật khẩu"
                variant="outlined"
              />
            </div>
            <div className={formInput}>
              <TextField
                onChange={this.handleChange}
                name="hoTen"
                fullWidth
                label="Họ Tên"
                variant="outlined"
              />
            </div>
            <div className={formInput}>
              <TextField
                onChange={this.handleChange}
                name="email"
                fullWidth
                label="Email"
                variant="outlined"
              />
            </div>
            <div className={formInput}>
              <TextField
                onChange={this.handleChange}
                name="soDT"
                fullWidth
                label="Số ĐT"
                variant="outlined"
              />
            </div>
            <div>
              <Button type="submit" variant="contained" color="primary">
                Đăng Ký
              </Button>
            </div>
          </form>
        </Container>
      </Fragment>
    );
  }
}

export default withStyles(styles)(Signup);
