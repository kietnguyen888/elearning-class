import React, { Component } from "react";
import Header from "../../components/Header";
import { connect } from "react-redux";
import { fetchCouse } from "../../store/action/course";
import { Box, Typography } from "@material-ui/core";
import Layout from "../../HOC/Layout";

class Detail extends Component {
  render() {
    const { maKhoaHoc, tenKhoaHoc, moTa, hinhAnh } = this.props.detail || {};
    return (
      <Layout>
        <div align="center">
          <Typography color="textSecondary" variant="h1">
            {tenKhoaHoc}
          </Typography>
          <Typography variant="h4" color="textPrimary">
            {maKhoaHoc}
          </Typography>
          <img style={{ height: 300 }} src={hinhAnh} alt="" />
          <Typography variant="body1">{moTa}</Typography>
        </div>
      </Layout>
    );
  }
  componentDidMount() {
    // 1.LAy param id tren url => maKhoaHoc
    const courseId = this.props.match.params.id;
    // chi component trong Route moi co this.props.match
    // 2.call api lay chi tiet khoa hoc => dispatch acsync action len middleware
    this.props.dispatch(fetchCouse(courseId));
  }
}

const mapStateToProps = (state) => {
  return {
    detail: state.detail.detail,
  };
};
export default connect(mapStateToProps)(Detail);
