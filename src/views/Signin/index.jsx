import { Button, Container, TextField } from "@material-ui/core";
import React, { Component } from "react";
import Header from "../../components/Header";
import { connect } from "react-redux";
import { signIn } from "../../store/action/authen";
class Signin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formValue: {
        taiKhoan: "",
        matKhau: "",
      },
    };
  }
  handleChange = (event) => {
    this.setState({
      formValue: {
        ...this.state.formValue,
        [event.target.name]: event.target.value,
      },
    });
  };
  handleSubmit = (event) => {
    event.preventDefault();
    this.props.dispatch(
      signIn(this.state.formValue, () => {
        this.props.history.push("/");
        // only components Route have history)
      })
    );
  };
  // handleDefaultLogin = () => {
  //   const userLogin = {
  //     taiKhoan: "anhkiet98",
  //     matKhau: "1234",
  //   };
  //   this.setState({
  //     formValue: {
  //       taiKhoan: userLogin.taiKhoan,
  //       matKhau: userLogin.matKhau,
  //     },
  //   });
  // };
  render() {
    return (
      <div>
        <Header />
        <div>
          <h1 style={{ textAlign: "center" }}>Đăng Nhập</h1>
          <Container maxWidth="sm">
            <form onSubmit={this.handleSubmit}>
              <div style={{ marginBottom: 30 }}>
                <TextField
                  onChange={this.handleChange}
                  name="taiKhoan"
                  fullWidth
                  label="Tài khoản"
                  variant="outlined"
                  // value={this.state.formValue.taiKhoan}
                />
              </div>
              <div style={{ marginBottom: 30 }}>
                <TextField
                  onChange={this.handleChange}
                  name="matKhau"
                  fullWidth
                  type="password"
                  label="Mật khẩu"
                  variant="outlined"
                  // value={this.state.formValue.taiKhoan}
                />
              </div>

              <div>
                <Button type="submit" variant="contained" color="primary">
                  Đăng Nhập
                </Button>
                <Button
                  type="button"
                  onClick={this.handleDefaultLogin}
                  color="primary"
                  variant="contained"
                >
                  Set default user
                </Button>
              </div>
            </form>
          </Container>
        </div>
      </div>
    );
  }
}

export default connect()(Signin);
