import React, { Component } from "react";
import Header from "../../components/Header";
import { Button } from "@material-ui/core";
import { Typography, Container, Grid, Box } from "@material-ui/core";
import Course from "../../components/Course";
import { connect } from "react-redux";
import { fetchCourses } from "../../store/action/course";
import Layout from "../../HOC/Layout";
class Home extends Component {
  render() {
    return (
      <Layout>
        <Typography component="h1" align="center" variant="h3">
          Danh sách khóa học
        </Typography>
        <Container maxWidth="lg">
          <Grid container spacing={3}>
            {/* mac dinh trong material ui 1 so la 8px => spacing ={3}=>24px */}
            {this.props.courses.map((item) => {
              return (
                <Grid key={item.maKhoaHoc} item xs={12} sm={6} md={3}>
                  <Course item={item} />
                </Grid>
              );
            })}
          </Grid>
        </Container>
      </Layout>
    );
  }

  //async await : biến asynchronous thành synchronous
  componentDidMount() {
    //side effect: nhung thay doi 1 state nam ngoai scope cua function
    this.props.dispatch(fetchCourses);
  }
}

const mapStateToProps = (state) => {
  return {
    courses: state.courses.courses,
  };
};
export default connect(mapStateToProps)(Home);
