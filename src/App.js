import React, { Component } from "react";
import Detail from "./views/Detail";
import Home from "./views/Home";
import Signin from "./views/Signin";
import Signup from "./views/Signup";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { fetchMe } from "./store/action/authen";
import { AuthRoute, PrivateRoute } from "./HOC/Route";
class App extends Component {
  render() {
    return (
      <div>
        <BrowserRouter>
          {/* <Header/> */}
          <Switch>
            <Route path="/detail/:id" component={Detail} />
            {/* If you've already signed in/ signup => redirect to Home */}
            <AuthRoute path="/signin" component={Signin} redirectPath="/" />
            <AuthRoute path="/signup" component={Signup} redirectPath="/" />

            <PrivateRoute
              exact
              path="/"
              component={Home}
              redirectPath="/signin"
            />
            {/* phai de cuoi cung (so sanh include) */}
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
  componentDidMount() {
    const token = localStorage.getItem("t");
    if (token) {
      this.props.dispatch(fetchMe);
    }
  }
}

export default connect()(App);
