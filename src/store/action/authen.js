import axios from "axios";
import { createAction } from ".";
import { actionType } from "./type";
export const signIn = (userLogin, callback) => {
  return async (dispatch) => {
    try {
      const res = await axios({
        url: "https://elearning0706.cybersoft.edu.vn/api/QuanLyNguoiDung/DangNhap",
        method: "POST",
        data: userLogin,
      });
      dispatch(createAction(actionType.SET_ME, res.data));
      localStorage.setItem("t", res.data.accessToken);
      callback();
    } catch (err) {
      console.log({ ...err });
      alert(err.response.data);
    }
  };
};

export const fetchMe = async (dispatch) => {
  try {
    const res = await axios({
      method: "POST",
      url: "https://elearning0706.cybersoft.edu.vn/api/QuanLyNguoiDung/ThongTinNguoiDung",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("t"),
      },
    });
    dispatch(createAction(actionType.SET_ME, res.data));
  } catch (err) {}
};
