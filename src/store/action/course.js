//tao ra async action de fetch ds khoa hoc
import axios from "axios";
import { createAction } from ".";
import { actionType } from "./type";
export const fetchCourses = async (dispatch) => {
  try {
    const res = await axios({
      url: "https://elearning0706.cybersoft.edu.vn/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc/",
      method: "GET",
      params: {
        MaNhom: "GP01",
      },
    });
    dispatch(createAction(actionType.SET_COURSES, res.data));
  } catch (err) {
    console.log(err);
  }
};

export const fetchCouse = (id) => {
  //return async action
  return async (dispatch) => {
    try {
      const res = await axios({
        url: "https://elearning0706.cybersoft.edu.vn/api/QuanLyKhoaHoc/LayThongTinKhoaHoc",
        method: "GET",
        params: {
          maKhoaHoc: id,
        },
      });
      //dispatch action len store de luu detail
      // (tao du lieu course detail tren reducer, createAction, actiontype)

      dispatch(createAction(actionType.SET_DETAIL, res.data));
    } catch (err) {
      console.log(err);
    }
  };
};
