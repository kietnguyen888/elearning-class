import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import courses from "./reducers/course";
import detail from "./reducers/detail";
import me from "./reducers/me";
const reducer = combineReducers({
  courses,
  detail,
  me,
});
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducer, composeEnhancers(applyMiddleware(thunk)));
export default store;
