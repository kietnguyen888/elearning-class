import React, { Component,Fragment } from "react";
import { NavLink } from "react-router-dom";
import {
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Button,
  withStyles,
} from "@material-ui/core";
import { ImportContacts } from "@material-ui/icons";
import styles from "./styles.js";
import { connect } from "react-redux";
class Header extends Component {
  render() {
    const { title, navLink, active } = this.props.classes;
    return (
      <AppBar position="static">
        <Toolbar>
          <IconButton edge="start" color="inherit" aria-label="menu">
            <ImportContacts />
          </IconButton>
          <Typography className={title} variant="h6">
            E-Learning
          </Typography>
          <NavLink
            className={navLink}
            exact
            activeClassName={active}
            to="/"
            href=""
          >
            Home
          </NavLink>
          {this.props.me ? (
            <Button style={{ marginLeft: 10 }} color="inherit">
              Hello, Kiet
            </Button>
          ) : (
            <Fragment>
               {/* Fragment dung de boc cac component  OR <>  */}
              <NavLink
                className={navLink}
                activeClassName={active}
                to="/signin"
              >
                Sign in
              </NavLink>
              <NavLink
                className={navLink}
                activeClassName={active}
                to="/signup"
              >
                Sign up
              </NavLink>
            </Fragment>
          )}
        </Toolbar>
      </AppBar>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    me: state.me,
  };
};
export default connect(mapStateToProps)(
  withStyles(styles, { withTheme: true })(Header)
);
//xu li ham style va bo vo component
//withTheme: true truyen theme vao style.js
