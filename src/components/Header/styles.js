const styles = (theme) => {
  return {
    "@global": {
      body: {
        backgroundColor: "#cecece",
      },
    },
    title: {
      flexGrow: 1,
      animation: "$Fade 0.5s infinite ",
    },
    navLink: {
      color: "#ffffff",
      marginLeft: "20px",
      // marginLeft:20
      opacity: 0.75,
      textDecoration: "none",
      "&:hover": {
        opacity: 1,
        color: "red",
      },
      // max-with:1280px
      [theme.breakpoints.down("md")]: {
        fontSize: 70,
      },
      // max-with:960px
      [theme.breakpoints.down("sm")]: {
        fontSize: 50,
      },
      // max-with:600px
      [theme.breakpoints.down("xs")]: {
        fontSize: 20,
      },
    },
    active: {
      opacity: 1,
    },
    "@keyframes Fade": {
      from: {
        opacity: 0,
      },
      to: {
        opacity: 1,
      },
    },
  };
};
export default styles;
