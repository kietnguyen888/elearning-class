import React, { Component } from "react";
import {
  CardActionArea,
  Card,
  CardMedia,
  CardContent,
  Typography,
  CardActions,
  Button,
  withStyles,
} from "@material-ui/core";
import { NavLink } from "react-router-dom";
import styles from "./style";
class Course extends Component {
  render() {
    const { hinhAnh, tenKhoaHoc, moTa, maKhoaHoc } = this.props.item;
    const { img, title } = this.props.classes;
    return (
      <Card>
        <CardActionArea>
          <CardMedia className={img} image={hinhAnh} classtitle={tenKhoaHoc} />
          <CardContent>
            <Typography
              className={title}
              gutterBottom
              variant="h5"
              component="h2"
            >
              {tenKhoaHoc}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              {moTa.substr(0, 100) + "..."}
            </Typography>
          </CardContent>
        </CardActionArea>
        <CardActions>
          <NavLink to={"/detail/" + maKhoaHoc}>
            <Button size="small" color="primary">
              Chi tiết
            </Button>
          </NavLink>
        </CardActions>
      </Card>
    );
  }
}

export default withStyles(styles, { withTheme: true })(Course);
