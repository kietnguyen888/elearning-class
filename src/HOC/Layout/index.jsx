import { Box, Typography } from "@material-ui/core";
import React, { Component } from "react";
import Header from "../../components/Header";

class Layout extends Component {
  render() {
    return (
      <Box bg="#cecece">
        <Header />
        {this.props.children}
        <Box
          bgcolor="#000"
          color="#fff"
          textAlign="center"
          paddingY="30px"
          marginTop="30px"
        >
          <Typography variant="h5" align="center">
            Footer
          </Typography>
        </Box>
      </Box>
    );
  }
}

export default Layout;
